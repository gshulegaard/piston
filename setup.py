#! /usr/bin/env python3
# -*- coding: utf-8 -*-
from setuptools import find_packages, setup

from piston import (__title__, __summary__, __url__, __version__, __author__,
                    __email__, __license__)
from piston.package import get_readme, UploadCommand


setup(
    name=__title__,
    version=__version__,
    description=__summary__,
    long_description=get_readme("README.rst"),
    long_description_content_type="text/x-rst; charset=UTF-8",
    author=__author__,
    author_email=__email__,
    url=__url__,
    packages=find_packages(exclude=["tests", "tests.*"]),
    license=__license__,
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: Implementation :: CPython",
    ],
    cmdclass={"upload": UploadCommand},
    install_requires=[
        "PyYAML==3.13"
    ]
)
