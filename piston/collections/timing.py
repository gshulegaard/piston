# -*- coding: utf-8 -*-
import time

from piston.context import Context


class TimeKeeper:
    def __init__(self):
        self.timings = []
        self.relative_base = time.time()

    def checkpoint(self, name=None):
        name = name if name is not None else Context().action_id
        now = time.time()
        self.timings.append(
            (name, now, now - self.relative_base, Context().action_duration)
        )

    @property
    def profile(self):
        profile = {}

        for _, timing in enumerate(self.timings):
            name, stamp, relative, duration = timing
            profile[name] = (stamp, f"{relative:.3f}s", f"{duration:.3f}s")

        return profile
