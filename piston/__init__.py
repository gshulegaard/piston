# -*- coding: utf-8 -*-
import os

from .context import Context
from .error import PistonError
from .lazyproperty import lazyproperty


__title__ = "piston"
__summary__ = "A collection of boilerplate for Python applications."
__url__ = "https://gitlab.com/gshulegaard/piston"

__version__ = "0.1.1"

__author__ = "Grant Hulegaard"
__email__ = "loki.labrys@gmail.com"

__license__ = "MIT"
__copyright__ = "Copyright (c) 2019 Grant Hulegaard"


here = os.path.dirname(__file__)
