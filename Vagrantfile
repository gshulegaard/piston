Vagrant.configure("2") do |config|
  mem  = 1024
  host = RbConfig::CONFIG['host_os']
  nfs = true

  # allocate more resources for the vm
  if host =~ /darwin/
    mem = `sysctl -n hw.memsize`.to_i / 1024 / 1024 / 2
  elsif host =~ /linux-gnu/
    mem = `head -n 1 /proc/meminfo | awk '{print $2;}'`.to_i / 1024 / 2

    # disable nfs for linux/ubuntu since there is a bug when tyring to use it
    nfs = false
  end

  # provider specific settings
  config.vm.provider "virtualbox" do |vbox|
    vbox.memory = mem
    vbox.cpus = 2
  end

  #config.vm.box = "ubuntu/trusty64"
  config.vm.box = "ubuntu/xenial64"
  config.vm.hostname = "vagrant"

  config.vm.network "private_network", ip: "192.168.50.50"

  if nfs == true
    # use NFS for performant file sharing
    # mount repo files for convenience
    config.vm.synced_folder ".", "/vagrant", type: "nfs"
  else
    # mount repo files for convenience
    config.vm.synced_folder ".", "/vagrant"
  end

  # install python3
  config.vm.provision "shell",
    inline: "sudo apt install -y python3 python3-setuptools python3-dev
      build-essential"

  # install pip
  config.vm.provision "shell",
    inline: "sudo easy_install pip3"

  # install requirements
  config.vm.provision "shell",
    inline: "sudo -u ubuntu pip3 install -r /vagrant/requirements"

end
